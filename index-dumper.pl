#!/usr/bin/perl
use strict;
use warnings;

# Lucene stuff by Robin H. Johnson <robbat2@gentoo.org>

use Lucene;
use Data::Dumper;

my $store = Lucene::Store::FSDirectory->getDirectory("data", 0);
my $reader = Lucene::Index::IndexReader->open($store);

# get number of docs in index
my $num_docs = $reader->numDocs();

for(my $i=0;$i<$num_docs; $i++) {
	# get the nth document
	my $doc = $reader->document($i);
	# This is missing in the Perl bindings :-(
	#my $fields = $doc->fields; 
	# So we have to either specify a field directly
	my $fields = $doc->get('md5');
	my $s = $doc->toString;
	print $s."\n";
}

$reader->close;
undef $reader;
