#!/usr/bin/perl
use strict;
use warnings;

# Lucene stuff by Robin H. Johnson <robbat2@gentoo.org>

use Lucene;
use Data::Dumper;

my $analyzer = new Lucene::Analysis::Standard::StandardAnalyzer();
my $store = Lucene::Store::FSDirectory->getDirectory("data", 0);
my $searcher = new Lucene::Search::IndexSearcher($store);
my $parser = new Lucene::QueryParser("filename", $analyzer);

# The numeric range queries don't work quite as you expect
# They run as strings, not numerics presently "size:[0 TO 9000]"
# http://lucene.apache.org/java/docs/queryparsersyntax.html
my $query = $parser->parse("distfile:akode* AND filename:m* AND isdistfile:0");
my $hits = $searcher->search($query);

# get number of results
 my $num_hits = $hits->length();

 # get fields and ranking score for each hit
 for (my $i = 0; $i < $num_hits; $i++) {
   my $doc = $hits->doc($i);
   my $score = $hits->score($i);
   my $path = $doc->get("path");
   my $size = $doc->get("size");
   my $md5 = $doc->get("md5");
   printf "%s %s %d\n",$path,$md5,$size;
 }

 # free memory and close searcher
 undef $hits;
 undef $query;
 undef $parser;
 undef $analyzer;
 $searcher->close();
 undef $searcher;
 undef $store;



